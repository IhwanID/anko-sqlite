package id.ihwan.sqliteandroid

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.update
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var oldNama = intent.getStringExtra("oldNama")
        var oldAlamat = intent.getStringExtra("oldAlamat")
        var oldHandphone = intent.getStringExtra("oldHandphone")

        if (oldAlamat.isNullOrBlank()) {
            buttonUpdate.isEnabled = false
        } else {
            buttonSimpan.isEnabled = false
            editTextNama.setText(oldNama)
            editTextAlamat.setText(oldAlamat)
            editTextHandphone.setText(oldHandphone)
        }

        buttonSimpan.setOnClickListener {
            addDataSantri()

            clearData()
        }

        buttonLihatData.setOnClickListener {

            startActivity<ListSantriActivity>()
        }

        buttonUpdate.setOnClickListener {
            database.use {
                update(
                    Santri.TABLE_SANTRI,
                    Santri.NAMA to editTextNama.text.toString(),
                    Santri.ALAMAT to editTextAlamat.text.toString(),
                    Santri.HANDPHONE to editTextHandphone.text.toString()
                )
                    .whereArgs(
                        "${Santri.NAMA} = {nama}",
                        "nama" to oldNama
                    ).exec()
            }

            clearData()
            toast("Data Diupdate")
        }
    }

    private fun addDataSantri() {
        database.use {
            insert(
                Santri.TABLE_SANTRI,
                Santri.NAMA to editTextNama.text.toString(),
                Santri.ALAMAT to editTextAlamat.text.toString(),
                Santri.HANDPHONE to editTextHandphone.text.toString()

            )

            toast("Data Berhasil Disimpan!")
        }
    }

    fun clearData() {
        editTextNama.text.clear()
        editTextAlamat.text.clear()
        editTextHandphone.text.clear()
    }
}
