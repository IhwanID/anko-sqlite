package id.ihwan.sqliteandroid

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_list_santri.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class ListSantriActivity : AppCompatActivity() {

    private lateinit var adapter: RVAdapter
    private var santri = ArrayList<Santri>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_santri)

        adapter = RVAdapter(this, santri)
        recyclerView.adapter = adapter

        getData()
        recyclerView.layoutManager = LinearLayoutManager(this)

    }

    private fun getData() {
        database.use {
            santri.clear()
            val result = select(Santri.TABLE_SANTRI)
            val dataSantri = result.parseList(classParser<Santri>())
            santri.addAll(dataSantri)
            adapter.notifyDataSetChanged()
        }
    }


}
