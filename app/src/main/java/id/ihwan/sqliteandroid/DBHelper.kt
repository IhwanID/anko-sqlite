package id.ihwan.sqliteandroid

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*


/**
 * Created by Ihwan ID on 24,October,2018.
 * Subscribe my Youtube Channel => https://www.youtube.com/channel/UCjntzibNSsjjIOh0HoP9vxw
 * mynameisihwan@gmail.com
 */
class DBHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "Santri.db", null, 1) {

    companion object {
        private var instance: DBHelper? = null
        @Synchronized
        fun getInstance(ctx: Context): DBHelper {

            if (instance == null) {
                instance = DBHelper(ctx.applicationContext)
            }
            return instance as DBHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(
            Santri.TABLE_SANTRI, true,
            Santri.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            Santri.NAMA to TEXT,
            Santri.ALAMAT to TEXT,
            Santri.HANDPHONE to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(Santri.TABLE_SANTRI, true)
    }


}


val Context.database: DBHelper
    get() = DBHelper.getInstance(applicationContext)